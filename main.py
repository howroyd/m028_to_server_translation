from helpers        import *
from payloadheader  import PayloadHeader
from devicepayloads import *
from testpacket     import *

import paho.mqtt.client as pahomqtt
import json
import time

class Devices:
    class Drager:
        topics  = Topics(DevIds.drager)
        header  = PayloadHeader(DevIds.drager)
        payload = DragerPayload()
        
    class Battery:
        topics  = Topics(DevIds.battery)
        header  = PayloadHeader(DevIds.battery)
        payload = BatteryPayload()
        
    class Beacon:
        topics  = Topics(DevIds.blebeacon)
        header  = PayloadHeader(DevIds.blebeacon)
        payload = BleBeaconPayload()
    
    # TODO If you add a new device class, append it to this list so it gets subscribed to and packets parsed
    @staticmethod
    def device_list() -> list:
        return [    __class__.Drager(), \
                    __class__.Battery(), \
                    __class__.Beacon()]

class MqttConnectionDetails:
    def __init__(self, host: str, port: int = 1883, keepalive: int = 60, client_id="", username=""):
        self.host = host
        self.port = port
        self.keepalive = keepalive
        self.client_id = client_id
        self.username = username

class Mqtt:
    instances = {}
    
    class Instance:
        ctr = 0
        # FIXME I have done this as multiple clients, which could cause an issue now I know it isnt trivial to add
        # usernames to the broker.  There is no need to have multiple clients if all data is on the same broker,
        # which I didn't assume it was, if we want to separate the JSON and the binary at the broker level, not
        # just the topic level.  The server I know uses wildcard subscribes, so this may confuse it, I don't know.
        
        def __init__(self, conn_details: MqttConnectionDetails, name=""):
            self.name = str(__class__.ctr) + "_" + name
            __class__.ctr += 1
            
            if 0 == len(conn_details.username):
                conn_details.username = self.name
            if 0 == len(conn_details.client_id):
                conn_details.client_id = self.name
            
            self.client = pahomqtt.Client(client_id=conn_details.client_id, clean_session=True, userdata=self.name, protocol=pahomqtt.MQTTv311, transport="tcp")
            self.client.on_connect = __class__.on_connect
            self.client.on_message = __class__.on_message
            
            self.client.username_pw_set(conn_details.username, password=None)
            
            self.client.connect(conn_details.host, conn_details.port, conn_details.keepalive)
            
            self.client.loop_start()
            
        @staticmethod
        def on_connect(client, userdata, flags, rc):
            if 0 == rc:
                print("MQTT connected (name=\"" + userdata + "\")")
            else:
                print("Connected with result code " + str(rc))
            
            for device in Devices.device_list():
                for topic in device.topics.as_list():
                    if userdata.endswith("binary"):
                        print("Subscribing to \"" + topic + "\"")
                        client.subscribe(topic)
                    elif userdata.endswith("json"):
                        json_topics = [TopicBuilder.json_in(topic), TopicBuilder.json_out(topic)]
                        for json_topic in json_topics:
                            print("Subscribing to \"" + json_topic + "\"")
                            client.subscribe(json_topic)

        # The callback for when a PUBLISH message is received from the server.
        @staticmethod
        def on_message(client, userdata, msg):
            #print(msg.topic + " " + str(msg.payload))
            
            if userdata.endswith("binary"):
                parsed = False
                
                for device in Devices.device_list():
                    if msg.topic == device.topics.as_dict()[Topics.Keys.value]: # Parse "value" packets
                        header  = PayloadHeader.from_bytes(msg.payload[:device.header.len_bytes])
                        payload = device.payload.from_bytes(msg.payload[device.header.len_bytes:])
                        if Mqtt.instances['json']:
                            topic_logging = TopicBuilder.json_out(msg.topic)
                            Mqtt.instances['json'].client.publish(topic_logging, json.dumps({'source': msg.topic, 'binary': msg.payload.hex()}, indent=4))
                            Mqtt.instances['json'].client.publish(topic_logging, make_json(header, payload, source=msg.topic))
                        parsed = True
                        break
                    
                if not parsed:
                    print("Packet was not parsed (ignored)")
                    print("Topic \"" + msg.topic + "\"")
            elif userdata.endswith("json"):
                parsed = False
                
                if msg.topic.startswith(TopicBuilder.json_out("")): # Logging topic
                    # Just log it
                    print("From topic \"" + msg.topic + "\"")
                    parsed_json = json.loads(msg.payload.decode("utf-8"))
                    print(json.dumps(parsed_json, indent=4))
                    parsed = True
                elif msg.topic.startswith(TopicBuilder.json_in("")): # Stuff you send to be converted to binary
                    # Incoming request, convert to binary and push out
                    topic_binary = TopicBuilder.json_in(msg.topic, strip = True)
                    topic_logging = TopicBuilder.json_out(topic_binary)
                    msg_as_json = json.loads(msg.payload.decode("utf-8"))
                    header_dict = msg_as_json['header']
                    payload_dict = msg_as_json['payload']
                    #client.publish(topic, header.to_bytes() + payload.to_bytes())
                    #parsed = True
                    for device in Devices.device_list():
                        if topic_binary == device.topics.as_dict()[Topics.Keys.value]: # Parse "value" packets
                            header  = PayloadHeader.from_dict(header_dict)
                            payload = device.payload.from_dict(payload_dict)
                            binary_msg = header.to_bytes() + payload.to_bytes()
                            
                            if Mqtt.instances['json']:
                                Mqtt.instances['json'].client.publish(topic_logging, make_json(header, payload, source=msg.topic))
                                Mqtt.instances['json'].client.publish(topic_logging, json.dumps({'source': msg.topic, 'binary': binary_msg.hex()}, indent=4))
                            if Mqtt.instances['binary']:
                                # pass
                                # FIXME ^ comment this out to prevent the script from publishing binary data as a hub, i.e. logging only
                                Mqtt.instances['binary'].client.publish(topic_binary, binary_msg)
                            parsed = True
                            break
                
                if not parsed:
                    print("Packet was not parsed (ignored)")
                    print("Topic \"" + msg.topic + "\"")

    class Handler:
        def __init__(self, binary_host: MqttConnectionDetails, \
                            ascii_host: MqttConnectionDetails = None, \
                            json_host: MqttConnectionDetails = None):
            Mqtt.instances['binary'] = Mqtt.Instance(binary_host, name="binary")
            Mqtt.instances['ascii']  = Mqtt.Instance(ascii_host, name="ascii") if ascii_host is not None else None # not implemented
            Mqtt.instances['json']   = Mqtt.Instance(json_host, name="json") if json_host is not None else None

#####################################################################
print("\nBEGIN MQTT\n")

def pub_battery_binary(client):
    topic_dict = Devices.Battery.topics.as_dict()
    topic = topic_dict[Topics.Keys.value]
    payload = PayloadHeader.Test().as_bytes() + BatteryPayload.Test().as_bytes()
    client.publish(topic, payload)

def pub_battery_json(client):
    topic_dict = Devices.Battery.topics.as_dict()
    topic = TopicBuilder.json_in(topic_dict[Topics.Keys.value])
    payload = BatteryTestJson.data
    client.publish(topic, payload)

# TODO insert server URL here along with client ID and username!
simons_mqtt_broker0 = MqttConnectionDetails("96.69.1.3", 1883, 60, client_id="MyClientID_0", username="username0")
simons_mqtt_broker1 = MqttConnectionDetails("96.69.1.3", 1883, 60, client_id="MyClientID_1", username="username0")
my_mqtt = Mqtt.Handler(binary_host=simons_mqtt_broker0, json_host=simons_mqtt_broker1)

# For testing
simons_mqtt_broker3 = MqttConnectionDetails("96.69.1.3", 1883, 60, client_id="MyClientID_TEST0", username="test0")
mqtt_test = pahomqtt.Client(client_id=simons_mqtt_broker3.client_id, clean_session=True, userdata=None, protocol=pahomqtt.MQTTv311, transport="tcp")
mqtt_test.username_pw_set(simons_mqtt_broker3.username, password=None)
mqtt_test.connect(simons_mqtt_broker3.host, simons_mqtt_broker3.port, simons_mqtt_broker3.keepalive)

pub_battery_binary(mqtt_test)
pub_battery_json(mqtt_test)

while True:
    time.sleep(10)
    print()
    pub_battery_binary(mqtt_test)
    pub_battery_json(mqtt_test)
