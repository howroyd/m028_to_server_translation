import time
from struct import pack, unpack
import binascii
from helpers import *

class PayloadHeader:
    len_bytes   = 28
    byte_format = '!BBBBBBBBBfff' # after DTC
    len_devid = 5
    common_data = {'name': 'PayloadHeader', 'len_bytes': len_bytes, 'byte_format': byte_format}       
        
    class DTC:
        len_bytes   = 7
        byte_format = '!HBBBBB'
        common_data = {'name': 'DTC', 'len_bytes': len_bytes, 'byte_format': byte_format}
        
        class Keys:
            year = 'year'
            month = 'month'
            day = 'day'
            hour = 'hour'
            minute = 'minute'
            second = 'second'
            
        class struct_time_compat:
            # This is a dirty workaround so you can *create* a time.struct_time like object from arbitrary data
            def __init__(self, tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec):
                self.tm_year = tm_year
                self.tm_mon = tm_mon
                self.tm_mday = tm_mday
                self.tm_hour = tm_hour
                self.tm_min = tm_min
                self.tm_sec = tm_sec
        
        def __init__(self, struct_time: time.struct_time = time.localtime()):
            # struct_time.tm_mon - 1 ..... Python is 1-12, not 0-11. Correct here
            self.data = dict([('common', self.common_data), \
                                (__class__.Keys.year, struct_time.tm_year), \
                                (__class__.Keys.month, struct_time.tm_mon - 1), \
                                (__class__.Keys.day, struct_time.tm_mday), \
                                (__class__.Keys.hour, struct_time.tm_hour), \
                                (__class__.Keys.minute, struct_time.tm_min), \
                                (__class__.Keys.second, struct_time.tm_sec)])
        
        @classmethod
        def from_bytes(cls, raw: bytes):
            data        = unpack(cls.byte_format, raw[:cls.len_bytes])
            
            time_tuple = __class__.struct_time_compat( \
                            tm_year = data[0], \
                            tm_mon = data[1] + 1, \
                            tm_mday = data[2], \
                            tm_hour = data[3], \
                            tm_min = data[4], \
                            tm_sec = data[5])
            
            return __class__(struct_time = time_tuple)
        
        @classmethod
        def from_asciihex(cls, asciihex: str):
            return cls.from_bytes(bytes.fromhex(asciihex))
        
        @classmethod
        def from_dict(self, dict_in):
            ret = __class__()
            for key in ret.data: # iterate through the expected keys (not the received ones!)
                ret.data[key] = dict_in[key]
            return ret
        
        def to_bytes(self) -> bytes:
            return pack(self.byte_format, \
                        self.data[__class__.Keys.year], \
                        self.data[__class__.Keys.month], \
                        self.data[__class__.Keys.day], \
                        self.data[__class__.Keys.hour], \
                        self.data[__class__.Keys.minute], \
                        self.data[__class__.Keys.second])

        def __str__(self):
            return str(self.data)
        
        def __repr__(self):
            dict_copy = dict(self.data)
            del dict_copy['common']
            return str(dict_copy)
        
        def as_dict(self):
            return self.data
        
        class Test:
            year            = "%04X" % 2022
            month           = "%02X" % 4
            day             = "%02X" % 21
            hour            = "%02X" % 15
            minute          = "%02X" % 29
            second          = "%02X" % 27
            
            def to_asciihex(self) -> str:
                return self.year + self.month + self.day + self.hour + self.minute + self.second
    
    class Keys:
        dtc = 'dtc'
        devid = 'devid'
        version = 'version'
        alert = 'alert'
        alert_extended = 'alert_extended'
        location_src = 'location_src'
        location_src = 'location_src'
        latitude = 'latitude'
        longitude = 'longitude'
        altitude = 'altitude'
    
    def __init__(self, devid: list[int] = [], \
                        dtc: time.struct_time   = time.localtime(), \
                        version: int            = 6, \
                        alert: int              = 1, \
                        alert_extended: int     = 1, \
                        location_src: int       = 1, \
                        latitude: float         = GPS.Locations.WTL.latitude, \
                        longitude: float        = GPS.Locations.WTL.longitude, \
                        altitude: float         = GPS.Locations.WTL.altitude):
        if isinstance(devid, str):
            # String to list of ints
            char_arr = list(devid)
            devid = [ord(asciiint) for asciiint in char_arr]        

        dtc_obj = dtc if isinstance(dtc, __class__.DTC) else __class__.DTC(dtc)
        devid_fixed_len = [0 for _ in range(self.len_devid - len(devid))] + devid[:self.len_devid]
        
        self.data = dict([('common', self.common_data), \
                            (__class__.Keys.dtc, dtc_obj), \
                            (__class__.Keys.devid, devid_fixed_len), \
                            (__class__.Keys.version, version), \
                            (__class__.Keys.alert, alert), \
                            (__class__.Keys.alert_extended, alert_extended), \
                            (__class__.Keys.location_src, location_src), \
                            (__class__.Keys.latitude, latitude), \
                            (__class__.Keys.longitude, longitude), \
                            (__class__.Keys.altitude, altitude)])
        
    @classmethod
    def from_bytes(cls, raw: bytes):
        header_bytes        = raw[:cls.len_bytes]
        header_after_dtc    = header_bytes[cls.DTC.len_bytes:]
        data                = unpack(cls.byte_format, header_bytes[cls.DTC.len_bytes:])
        
        return __class__(devid              = list(data[0:5]), \
                            dtc             = __class__.DTC.from_bytes(header_bytes), \
                            version         = data[5], \
                            alert           = data[6], \
                            alert_extended  = data[7], \
                            location_src    = data[8], \
                            latitude        = data[9], \
                            longitude       = data[10], \
                            altitude        = data[11])
    
    @classmethod
    def from_asciihex(cls, asciihex: str):
        return cls.from_bytes(bytes.fromhex(asciihex))
    
    @classmethod
    def from_dict(self, dict_in):
        ret = __class__()
        for key in ret.data: # iterate through the expected keys (not the received ones!)
            if __class__.Keys.dtc == key:
                ret.data[key] = __class__.DTC.from_dict(dict_in[key]) # special case, we need a DTC obj here
            else:
                ret.data[key]   = dict_in[key]
        return ret
     
    def to_bytes(self) -> bytes:
        return self.data[__class__.Keys.dtc].to_bytes() + pack(self.byte_format, \
                                           *self.data[__class__.Keys.devid], \
                                            self.data[__class__.Keys.version], \
                                            self.data[__class__.Keys.alert], \
                                            self.data[__class__.Keys.alert_extended], \
                                            self.data[__class__.Keys.location_src], \
                                            self.data[__class__.Keys.latitude], \
                                            self.data[__class__.Keys.longitude], \
                                            self.data[__class__.Keys.altitude])
    
    def __str__(self):
        return str(self.data)
    
    def as_dict(self):
        serialised = self.data
        serialised[__class__.Keys.dtc] = serialised[__class__.Keys.dtc].as_dict()
        return serialised
    
    def timestamp(self):
        return self.data[__class__.Keys.dtc]
    
    class Test:
        devid           = '0a0b0c0d0e'
        version         = "%02X" % 6
        alert           = "%02X" % 3
        alert_extended  = "%02X" % 0
        location_src    = "%02X" % 1
        latitude        = float_to_ascii_hex(GPS.Locations.WTL.latitude)
        longitude       = float_to_ascii_hex(GPS.Locations.WTL.longitude)
        altitude        = float_to_ascii_hex(GPS.Locations.WTL.altitude)
        
        def to_asciihex(self) -> str:
            return PayloadHeader.DTC.Test().to_asciihex() + self.devid + self.version + self.alert + self.alert_extended + self.location_src + self.latitude + self.longitude + self.altitude
        
        def as_bytes(self) -> bytes:
            return ascii_hex_to_bytes(self.to_asciihex())