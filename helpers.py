from struct import pack, unpack
import json

class TopicBuilder:
    @staticmethod
    def pair_topic(dev_str: str) -> str:
        return dev_str + "/" + "add"
    
    @staticmethod
    def unpair_topic(dev_str: str) -> str:
        return dev_str + "/" + "del"
    
    @staticmethod
    def value_topic(dev_str: str) -> str:
        return dev_str + "/" + "val"
    
    @staticmethod
    def alert_topic(dev_str: str) -> str:
        return dev_str + "/" + "alt"
    
    @staticmethod
    def prefix_topic(topic: str, prefix: str, strip: bool = False) -> str:
        if strip:
            return topic.lstrip(prefix)
        else: 
            return prefix + topic
        
    @staticmethod
    def json_out(topic: str, strip: bool = False) -> str:
        return __class__.prefix_topic(topic, "json_out/", strip = strip)
    
    @staticmethod
    def json_in(topic: str, strip: bool = False) -> str:
        return __class__.prefix_topic(topic, "json_in/", strip = strip)

def float_to_ascii_hex(f: float) -> str:
    return hex(unpack('<I', pack('<f', f))[0]).lstrip('0x')

def ascii_hex_to_bytes(*args):
    return bytes.fromhex(''.join(args))

class Topics:
    class Keys:
        pair = "pair"
        unpair = "unpair"
        value = "value"
        alert = "alert"        
    
    def __init__(self, devid: str):
        self.data = { \
            #__class__.Keys.pair: TopicBuilder.pair_topic(devid), \
            #__class__.Keys.unpair: TopicBuilder.unpair_topic(devid), \
            __class__.Keys.value: TopicBuilder.value_topic(devid), \
            #__class__.Keys.alert: TopicBuilder.alert_topic(devid), \
        }
        print(list(self.data.values()))
        
    def as_list(self):
        return list(self.data.values())
    
    def as_dict(self):
        return self.data
        
class GPS:
    class Locations:
        class WTL:
            latitude: float         = 52.595402
            longitude: float        = -1.2145739
            altitude: float         = 94.0
            
def make_json(header, payload, source = "", indent = None):
    packet = {'source': source, 'header': header.as_dict(), 'payload':payload.as_dict()}
    return json.dumps(packet, indent=indent)