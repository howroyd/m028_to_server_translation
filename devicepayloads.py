from helpers import *
import struct

class DevIds:
    drager  = "drg"
    battery = "pow"
    blebeacon = "bea"
    
class DragerPayload:
    devid       = DevIds.drager
    len_bytes   = 5
    byte_format = '!fB'
    common_data = {'name': 'DragerPayload', 'devid': devid, 'len_bytes': len_bytes, 'byte_format': byte_format}
    
    class Keys:
        oxygen_level = 'oxygen_level'
        alert_status = 'alert_status'

    def __init__(self, oxygen_level: float = 0.0, alert_status: int = 0):
        self.data = dict([('common', self.common_data), \
                            (__class__.Keys.oxygen_level, oxygen_level), \
                            (__class__.Keys.alert_status, alert_status)])
        
    @classmethod
    def from_bytes(cls, raw: bytes):
        data = unpack(cls.byte_format, raw[:cls.len_bytes])
        return __class__(data[0], data[1])
    
    @classmethod
    def from_asciihex(cls, asciihex: str):
        return cls.from_bytes(bytes.fromhex(asciihex))
    
    @classmethod
    def from_dict(self, dict_in):
        ret = __class__()
        for key, value in ret.data: # iterate through the expected keys (not the received ones!)
            ret.data[key] = dict_in[key]
        return ret

    def to_bytes(self) -> bytes:
        return pack(self.byte_format, \
                        self.data[__class__.Keys.oxygen_level], \
                        self.data[__class__.Keys.alert_status])

    def __str__(self):
        return str(self.data)
    
    def as_dict(self):
        return self.data
    
    class Test:
        oxygen_level    = float_to_ascii_hex(96.5)
        status          = "%02X" % 2
        
        def __repr__(self):
            return self.oxygen_level + self.status
        
        def as_bytes(self):
            return ascii_hex_to_bytes(self.oxygen_level, self.status)

class BatteryPayload:
    devid       = DevIds.battery
    len_bytes   = 3
    byte_format = '!HB'
    common_data = {'name': 'BatteryPayload', 'devid': devid, 'len_bytes': len_bytes, 'byte_format': byte_format}

    class Keys:
        battery_pc = 'battery_pc'
        status     = 'status'

    def __init__(self, battery_pc: int = 0, status: int = 0):
        self.data = dict([('common', self.common_data), \
                            (__class__.Keys.battery_pc, battery_pc), \
                            (__class__.Keys.status, status)])
        
    @classmethod
    def from_bytes(cls, raw):        
        data = unpack(cls.byte_format, raw[:cls.len_bytes])
        return __class__(data[0], data[1])
    
    @classmethod
    def from_asciihex(cls, asciihex: str):
        return cls.from_bytes(bytes.fromhex(asciihex))
    
    @classmethod
    def from_dict(self, dict_in):
        ret = __class__()
        for key in ret.data: # iterate through the expected keys (not the received ones!)
            ret.data[key] = dict_in[key]
        return ret

    def to_bytes(self) -> bytes:
        return pack(self.byte_format, \
                        self.data[__class__.Keys.battery_pc], \
                        self.data[__class__.Keys.status])

    def __str__(self):
        return str(self.data)
    
    def as_dict(self):
        return self.data
    
    class Test:
        battery_pc = "%04X" % 783 # in deci-percent. (e.g. 783 === 78.3%)
        status     = "%02X" % 2
        
        def __repr__(self):
            return self.battery_pc + self.status
        
        def as_bytes(self):
            return ascii_hex_to_bytes(self.battery_pc, self.status)
        
class BleBeaconPayload:
    devid       = DevIds.blebeacon
    #len_bytes   = None # dynamic length
    #byte_format = '!B......'   # dynamic length
    common_data = {'name': 'BleBeaconPayload', 'devid': devid}#, 'len_bytes': len_bytes, 'byte_format': byte_format}

    class Keys:
        n_beacons = 'n_beacons'
        beacon_list = 'beacon_list'
        
    class Beacon:
        len_bytes   = 6
        byte_format = '!HHbb'   # dynamic length
        common_data = {'name': 'Beacon', 'len_bytes': len_bytes, 'byte_format': byte_format}
        
        class Keys:
            major = "major"
            minor = "minor"
            rssi_cal = "rssi_cal"
            rssi_obs = "rssi_obs"
        
        def __init__(self, major: int = 0, minor: int = 0, rssi_calibrated: int = 0, rssi_observed: int = 0):
            self.data = dict([('common', self.common_data), \
                                (__class__.Keys.major, major), \
                                (__class__.Keys.minor, minor), \
                                (__class__.Keys.rssi_cal, rssi_calibrated), \
                                (__class__.Keys.rssi_obs, rssi_observed)])

        @classmethod
        def from_bytes(cls, raw):
            data = unpack(cls.byte_format, raw[:cls.len_bytes])
            return __class__(data[0], data[1], data[2], data[3])
        
        @classmethod
        def from_asciihex(cls, asciihex: str):
            return cls.from_bytes(bytes.fromhex(asciihex))
        
        @classmethod
        def from_dict(self, dict_in):
            ret = __class__()
            for key in ret.data: # iterate through the expected keys (not the received ones!)
                ret.data[key] = dict_in[key]
            return ret
        
        def to_bytes(self) -> bytes:
            return pack(self.byte_format, \
                        self.data[__class__.Keys.major], \
                        self.data[__class__.Keys.minor], \
                        self.data[__class__.Keys.rssi_cal], \
                        self.data[__class__.Keys.rssi_obs])
        
        def __str__(self):
            return str(self.data)
    
        def as_dict(self):
            return self.data
        
        class Test:
            major    = "%02X" % 123
            minor    = "%02X" % 45
            rssi_cal = struct.pack("!b", 67).hex().upper() # Note 2's compliment
            rssi_obs = struct.pack("!b", -89).hex().upper()
            
            def __repr__(self):
                return self.major + self.minor + self.rssi_cal + self.rssi_obs
            
            def as_bytes(self):
                return ascii_hex_to_bytes(self.major, self.minor, self.rssi_cal, self.rssi_obs)

    def __init__(self, beacon_list = []):
        self.n_beacons = len(beacon_list)
        if 0 < len(beacon_list):
            if isinstance(beacon_list[0], __class__.Beacon):
                beacon_list = [x.as_dict() for x in beacon_list]
        self.data = dict([('common', self.common_data), \
                            (__class__.Keys.beacon_list, beacon_list)])
        
    @classmethod
    def from_bytes(cls, raw):
        n_beacons = unpack("!B", raw[:1])
        beacons = []
        for i in range(0, n_beacons):
            idx_start = 1 + (i * __class__.Beacon.len_bytes)
            idx_end = idx_start + __class__.Beacon.len_bytes
            beacons.append(__class__.Beacon.from_bytes(raw[idx_start:idx_end]))
        return __class__(beacon_list = [x.as_dict() for x in beacons] if 0 < n_beacons else beacons)
    
    @classmethod
    def from_asciihex(cls, asciihex: str):
        return cls.from_bytes(bytes.fromhex(asciihex))
    
    @classmethod
    def from_dict(self, dict_in):
        ret = __class__()
        for key in ret.data: # iterate through the expected keys (not the received ones!)
            ret.data[key] = dict_in[key]
        return ret
    
    def to_bytes(self) -> bytes:
        n_beacons = len(self.data[__class__.Keys.beacon_list])
        ret = pack('!B', n_beacons)
        for this_beacon in self.data[__class__.Keys.beacon_list]:
            ret += this_beacon.to_bytes()
        return ret 

    def __str__(self):
        return str(self.as_dict())
    
    def as_dict(self):
        out_dict = self.data
        out_dict.update((__class__.Keys.n_beacons, len(self.data[__class__.Keys.beacons])))
        return out_dict
    
    class Test:
        list_len  = 3
        n_beacons = "%01X" % list_len
        
        def __init__(self):
            self.beacon = BleBeaconPayload.Beacon.Test()
        
        def __repr__(self):
            return self.n_beacons + "".join(repr(self.beacon) for x in range(0, self.list_len))
        
        def as_bytes(self):
            return ascii_hex_to_bytes(repr(__class__()))